# django2_website

This Django project has one application.
A small working web application. It has features for post drafts and 
their publication including deletion of posts that we no longer want

This application is the same as the one available at https://bitbucket.org/chrek/django2_postgresql_heroku/src
The only difference is that here we are using Sqlite3 instead of Postgres

The site is hosted on PythonAnywhere and can be accessed from:
https://chrek.pythonanywhere.com/

PythonAnywhere (www.pythonanywhere.com) is a service for running Python code on servers "in the cloud". 

The source code is available from https://bitbucket.org/chrek/django2_website/src

----

The main features that have currently been implemented are:

* The blog needs a username and password to log in. 
* You need to be logged in to add, edit, publish or delete posts, and can log out again.
* Features for post drafts
* Blog readers have the possibilty of sending feedbacks by commenting on published posts. However not all comments are approved

## Models: 
1. Post 
2. Comment

## Web Pages:
* login.html: used for logging in
* post_list.html: for publishing available posts
* post_detail.html: displays the details of a post
* post_new.html: used to create or add a post or a comment
* post_edit.html: used to edit a post or a comment
* post_draft_list.html: Page with list of unpublished posts
* add_comment_to_post.html: Page to add comments to published posts

## Template Inheritance:

Every page of the website inherits from a base template (base.html). 

## Views:

Views are supposed to : connect models and templates

## Static Files:

CSS, Bootstrap 4, Fontawaresome, Font called Lobster from Google Fonts (https://www.google.com/fonts)

## Forms

* A form to create/add and edit blog posts. To create a new Post form, we need to call PostForm() and pass it to the template.
* Another form to create comments on blog posts.

## Database: 

Sqlite3

----

## References

1.	[djangogirls](https://tutorial.djangogirls.org/en/)
2.  [djangoproject](https://docs.djangoproject.com/en/2.1/intro/)